from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^about/$', views.About.as_view(), name='about'),
    url(r'^register/$',views.register,name='register'),
    url(r'^restricted/',views.restricted, name='restricted'),
    url(r'^users_list/$',views.users_list, name='users_list'),
    url(r'^login/$',views.user_login, name='login'),
    url(r'^logout/$',views.user_logout, name='logout'),
    url(r'^goto/$',views.track_url, name='goto'),
	url(r'^add_category/$', views.Add_Category.as_view(), name='add_category'),
    url(r'^profile/(?P<username>[\w\-]+)/$', views.user_profile, name='user_profile'),
    url(r'^category/(?P<category_name_slug>[\w\-]+)/$', views.category, name='category'),
    url(r'^category/(?P<category_name_slug>[\w\-]+)/add_page/$', views.add_page, name='add_page'),
    url(r'^like_category/$', views.like_category, name='like_category'),
    url(r'^suggest_category/$', views.suggest_category, name='suggest_category'),

)

#coding: utf-8
from django.shortcuts import render, redirect
from rango.models import *
from django.contrib.auth.decorators import login_required


@login_required
def profile(request, username):
    context = {}
    print  'cheguei aqui'
    try:
        u = User.objects.get(username=username)
    except:
        return  redirect('/rango/usuarios')
    try:
        up = UserProfile.objects.get(user=u)
    except:
        up = None
    context['u'] = u
    context['userprofile'] = up

    return render(request, 'rango/profile.html', context)
#coding: utf-8
from django.shortcuts import render, redirect
from index import *
from rango.forms import CategoryForm, PageForm, UserForm, UserProfileForm
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from datetime import datetime
from django.views.generic import *
from LoginRequired import *




class Add_Category(LoginRequiredMixin, View):
    template_name = 'rango/add_category.html'

    def get(self,request):
        context_dict = {}
        # if request.method != "POST":
        form = CategoryForm
        context_dict['form'] = form
        return render(request, self.template_name , context_dict)

    def post(self, request):

        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            print form.errors

        # return render(request, self.template , context_dict)
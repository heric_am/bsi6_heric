#coding: utf-8
from django.shortcuts import render, redirect
from rango.models import *
from django.contrib.auth.decorators import login_required



@login_required
def usuarios(request):
    context = {}
    users = User.objects.order_by('username')
    context['users'] = users
    return render(request, 'rango/usuarios.html', context)

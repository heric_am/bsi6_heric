#coding: utf-8

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from rango.models import Page, User

@login_required
def restricted(request):
    return render(request,'rango/restricted.html',{})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/rango/')

def track_url(request):
    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']
            try:
                page = Page.objects.get(id=page_id)
                page.views += 1
                page.save()
                return redirect(page.url)
            except:
                pass
    return redirect('/rango/')

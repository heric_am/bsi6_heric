#coding: utf-8
from rango.models import *
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required



@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/rango/')
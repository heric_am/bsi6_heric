#coding: utf-8
from django.shortcuts import render, redirect
from rango.models import *

def track_url(request):
    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']
            try:
                page = Page.objects.get(id=page_id)
                page.views += 1
                page.save()
                return redirect(page.url)

            except:
                pass
        return redirect('/rango/')
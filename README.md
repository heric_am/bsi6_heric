# README #

###Detalhes sobre a instalação projeto.###

##Comandos necessarios para instalação##

* Pip e biblioteca python: `apt-get install python-pip python3-pip`
* Biblioteca de desenvolvimento: `python apt-get install python-dev`
* Ambiente virtual para evitar incompatibilidades via pip:
`pip install virtualenv`
`pip install virtualenvwrapper`
`pip install virtualenvwrapper-win`
* Pillow para upload de media e imagens (ATENÇÃO o python-dev é necessario para a instalação do pillow!!!): `pip install pillow`
* Instalação do django:  `pip install django==1.7`
* Instalação do virtualenv via apt: `sudo apt-get install virtualenv`
* Instalação do libpq: `sudo apt-get install libpq-dev`
* As instalações atravez do pip devem ser realizadas dentro da virtualenv
* Lembre-se de adicionar o virtual env source ao bashrc.
* Use `pip freeze` para verificar a versão do que esta instalado na sua virtualenv

##Pycharm##

* Para instalação do pycharm no ubuntu 15.04 é necessario a instalação da aplicação java: `apt-get install openjdk-8-jre-headless`
* Caso você ja tenha o java instalado em sua maquina, basta baixar o pycham do site https://www.jetbrains.com/pycharm/
* A instalação é relativamente simples, basta descompactar o programa e usar o comando ./nome do instalador
* ATENÇÃO, é possivel enfrentar problemas ao utilizar o pycharm e ferramentas relacionadas ao posgis e quantum gis em ambiente virutualizado.
* O pycharm oferece integração com o git, a criação de uma conta no github ou bitbucket é recomendada.
* Criar o projeto inicial usando o pycharm permitira a integração do virtualenv e terminal no proprio pycharm, o que é extremamente util
* Licença utilizada pelo IFC - usuário :
`Instituto Federal Catarinense`
* Licença chave:
`488075-30052014
00000"zcyIZ!vklKZe4mnewqb7Pd3S
ngn90tKswdqvlANfT0"QpxcAMmOrNG
1cF!y"lzagjJ9Vfh3ZuBLeRUzn5a0q`

## Configuração do git ##

* Selecione a pasta onde o git será configurado, normalmente, tratasse da pasta onde seu projeto foi ou sera criado
* Iniciando git `git init`
* Configuração `git remote add origin endereço do git`
* Crie um arquivo com o nome dos contribuidores do projeto `echo "nome" >> contributors.txt`
* Adicione o arquivo ao git: `contributors.txt`
* Faça o commit inicial `git commit -m "Nome do commit"`
* Use o git push para enviar o commit ao bitbucket 'git push -u origin master'
* Após a criação do seu projeto, crie o arquivo .gitignore para não enviar arquivos indesejados ao repositorio. é sugerido que o banco de dados seja incluido no git ignore.



## Tango with django 1.7 ##

Para o desenvolvimento deste projeto, foi-se utilizado o tutorial: tango with django. O tutorial é bem estruturado e funciona muito bem com o django 1.7. Este fornece uma quantidade grande de ferramentas
que podem ser usadas futuramente para outros projetos.
Algumas funcionalidades merecem destaque:

Utilização do slugfy:

A utilização do slugfy permite ao desenvolvedor reutilizar as variaveis dos links em futuras urls. Isso reduz a necessidade de novas urls estaticas. É extremamente recomendavem quando os usuarios podem criar novas paginas dentro do sistema.

A explicação para a utilização do slug pode ser encontrada em: www.tangowithdjango.com/www.tangowithdjango.com/book17/chapters/models_templates.html

Criação do base.html:

Este recurso utiliza uma template base para a criação das demais templates. Permite a utilização de codigo padrão, isso faz com que não seja necessario escrever duas templates pra desempenhar o mesmo papel

explicação para a criação do base html pode ser encontrada em: www.tangowithdjango.com/www.tangowithdjango.com/book17/chapters/templates.html

Forms.py:

Este recurso facilita a criação de formularios e utilização do banco de dados, com poucas linhas é possivel criar o formulario e envialo ao banco de dados sem a utilização da linguagem pura sql.

Para mais detalhes acesse: www.tangowithdjango.com/www.tangowithdjango.com/book17/chapters/forms.html

## Comandos para criação do banco de dados ##

* `python manage.py migrate`
* `python manage.py makemigrations`
* `python manage.py createsuperuser`
* `python populate_rango.py`
* `python manage.py runserver`

##Atalhos pycharm##

* Endentação automatica: ctrl + alt + i
* Substituição de campos com mesmo nome: ctrl + r
* Verificar fonte do codigo: Ctrl + botão esquedo
* Procurar palavra: ctrl + f
* Desfazer: ctrl + z
* Refazer: ctrl + shift + z
* Duplicar linha ctrl + d
* Comentar varias linhas ctrl + /
